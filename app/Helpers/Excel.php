<?php 

namespace App\Helpers;

class Excel {

	public static function getExcelLikeColumnName($number = 0) : string
	{
		$numeric = ($number - 1) % 26;
		$letter = chr(65 + $numeric);
		$number2 = intval(($number - 1) / 26);

		if ($number2 > 0) {

			return self::getExcelLikeColumnName($number2) . $letter;

		}else{

			return $letter;
		}
	}
}
