<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VerifyMatrixValidity implements Rule
{
    private $first_matrix_is_valid = true;
    private $second_matrix_is_valid = true;

    private $first_matrix_column_length = 0;
    private $second_matrix_column_length = 0;

    private $first_matrix_row_length = 0;
    private $second_matrix_row_length = 0;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $first_matrix = request('first_matrix', []);
        $second_matrix = request('second_matrix', []);

        $this->first_matrix_row_length = count($first_matrix);
        $this->second_matrix_row_length = count($second_matrix);

        $this->first_matrix_column_length = count($first_matrix[0] ?? []);
        $this->second_matrix_column_length = count($second_matrix[0] ?? []);

        foreach ($first_matrix as $row) {

            if (count($row) !== $this->first_matrix_column_length) {

                $this->first_matrix_is_valid = false;
                break;
            }
        }

        foreach ($second_matrix as $row) {

            if (count($row) !== $this->second_matrix_column_length) {

                $this->second_matrix_is_valid = false;
                break;
            }
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (! $this->first_matrix_is_valid || ! $this->second_matrix_is_valid) {

            return false;
        }

        if ($this->first_matrix_column_length != $this->second_matrix_row_length) {

            return false;
        }

        return true;
      
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The given set of matrices are not compatible.';
    }
}
