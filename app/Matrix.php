<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    public function setFirstMatrixAttribute($value)
    {
        $this->attributes['first_matrix'] = json_encode($value);
    }

    public function setSecondMatrixAttribute($value)
    {
        $this->attributes['second_matrix'] = json_encode($value);
    }

    public function setResultAttribute($value)
    {
        $this->attributes['result'] = json_encode($value);
    }

    public function setExcelResultAttribute($value)
    {
        $this->attributes['excel_result'] = json_encode($value);
    }

    public function getFirstMatrixAttribute($value)
    {
        return json_decode($value);
    }

    public function getSecondMatrixAttribute($value)
    {
        return json_decode($value);
    }

    public function getResultAttribute($value)
    {
        return json_decode($value);
    }

    public function getExcelResultAttribute($value)
    {
        return json_decode($value);
    }
}
