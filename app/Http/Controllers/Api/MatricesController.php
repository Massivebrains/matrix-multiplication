<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MatricesRequest;
use App\User;
use App\Helpers\Excel;
use App\Matrix;

class MatricesController extends Controller
{
	public function index()
	{
		$matrices = Matrix::get();

		return response()->json([

			'message' => 'Matrices retrieved successfully',
			'data' => $matrices
		]);
	}

	public function compute(MatricesRequest $request)
	{
		$first_matrix = request('first_matrix');
		$second_matrix = request('second_matrix');

		$matrix = Matrix::query()
		->whereFirstMatrix(json_encode($first_matrix))
		->whereSecondMatrix(json_encode($second_matrix))
		->first();

		if ($matrix) {

			return response()->json([

				'message' => 'Matrices product successfully computed',
				'data' => $matrix
			]);
		}

		$new_matrix 	= [];

		for ($row = 0; $row < count($first_matrix); $row++) {

			for ($column = 0; $column < count($second_matrix[0]); $column++) {

				$new_matrix[$row][$column] = 0;

				for ($item = 0; $item < count($first_matrix[$row]); $item++) {

					$new_matrix[$row][$column]+= $first_matrix[$row][$item] * $second_matrix[$item][$column];
				}
			}
		}

		$excel_like_matrix 	= [];

		for ($row = 0; $row < count($new_matrix); $row++) {

			for ($column = 0; $column<count($new_matrix[$row]); $column++) {

				$excel_like_matrix[$row][$column] = Excel::getExcelLikeColumnName($new_matrix[$row][$column]);
			}
		}

		$matrix = Matrix::create([

			'first_matrix' => $first_matrix,
			'second_matrix' => $second_matrix,
			'result' => $new_matrix,
			'excel_result' => $excel_like_matrix
		]);

		return response()->json([

			'message' => 'Matrices product successfully computed',
			'data' => $matrix

		], 201);
	}

}
