<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\VerifyMatrixValidity;

class MatricesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'first_matrix'        => 'required|array',
            'second_matrix'       => 'required|array',
            'first_matrix.*'      => 'array',
            'second_matrix.*'     => 'array',
            'first_matrix.*.*'    => 'numeric',
            'second_matrix.*.*'   => 'numeric',
            'first_matrix'        => [new VerifyMatrixValidity]
        ];
    }

    public function messages()
    {
        return [

            'first_matrix.*.*.numeric' => 'All items in the first matrix must be a number.',
            'second_matrix.*.*.numeric'  => 'All items in the second matrix must be a number.',
        ];
    }
}
