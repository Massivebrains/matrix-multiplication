<?php

namespace Tests\Feature\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Helpers\Excel;

class ExcelTest extends TestCase
{
     /** @test */
    public function it_returns_correct_excel_column_name()
    {
        $this->assertEquals('A', Excel::getExcelLikeColumnName(1));
        $this->assertEquals('Z', Excel::getExcelLikeColumnName(26));
        $this->assertEquals('AA', Excel::getExcelLikeColumnName(27));
        $this->assertEquals('AB', Excel::getExcelLikeColumnName(28));
    }
}
