<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class MatricesControllerTest extends TestCase
{
    public $user;
    public $header;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->header = [

            'HTTP_AUTHORIZATION'    => base64_encode($this->user->email.':password'),
            'PHP_AUTH_USER'         => $this->user->email,
            'PHP_AUTH_PW'           => 'password'

        ];
    }
    
    /** @test */
    public function it_denies_access_when_not_authenticated()
    {
        $response = $this->json('GET', '/api/v1/matrices')
        ->assertStatus(401)
        ->assertJson([

            'status' => 'unauthorized',
            'data'   => [

                'message' => 'Unauthorized.'
            ]
        ]);

    }

    /** @test */
    public function it_fails_to_compute_matrices_on_validation_error()
    {
        $payload = [

            [
                'first_matrix' => [

                    ['not-numeric']
                ],

                'second_matrix' => [

                    ['not-numeric']
                ]
            ],

            [
                'first_matrix' => [

                    [1, 4, 5],
                    [3, 4]
                ],

                'second_matrix' => [

                    [1, 5, 3, 4],
                    [5, 2, 4, 1],
                    [3, 4, 6, 2]
                ]
            ],

            [
                'first_matrix' => [

                    [1, 4, 5],
                    [3, 4, 1]
                ],

                'second_matrix' => [

                    [1, 5, 3, 4],
                    [1, 5, 3, 4],
                    [1, 5, 3, 4],
                    [1, 5, 3, 4],
                ]
            ]
        ];

        foreach ($payload as $row) {

            $response = $this->json('POST', '/api/v1/matrices/compute', $row, $this->header)
            ->assertStatus(422)
            ->assertJsonStructure([

                'status',
                'data' => [

                    'message'
                ]
            ]);
        }

    }

    /** @test */
    public function it_computes_matrices_successfully()
    {
        $payload = [

            'first_matrix' => [

                [2, 3, 4],
                [6, 2, 1],
                [4, 1, 2]
            ],

            'second_matrix' => [

                [1, 4, 8],
                [-1, 6, 0],
                [7, 2, 12]
            ]
        ];

        $response = $this->json('POST', '/api/v1/matrices/compute', $payload, $this->header)
        ->assertStatus(201)
        ->assertJson([

            'data' => [

                'result' => [

                    [27, 34, 64],
                    [11, 38, 60],
                    [17, 26, 56]
                ]
            ]
        ])
        ->assertJsonStructure([

            'message',
            'data'
        ]);
        
        $this->assertDatabaseHas('matrices', [

            'first_matrix' => json_encode($payload['first_matrix'])

        ]);

    }

    /** @test */
    public function it_computes_matrices_but_returns_record_from_cache()
    {
        $payload = [

            'first_matrix' => [

                [2, 3, 4],
                [6, 2, 1],
                [4, 1, 2]
            ],

            'second_matrix' => [

                [1, 4, 8],
                [-1, 6, 0],
                [7, 2, 12]
            ]
        ];

        $response = $this->json('POST', '/api/v1/matrices/compute', $payload, $this->header)
        ->assertStatus(200)
        ->assertJson([

            'data' => [

                'result' => [

                    [27, 34, 64],
                    [11, 38, 60],
                    [17, 26, 56]
                ]
            ]
        ])
        ->assertJsonStructure([

            'message',
            'data'
        ]);
        
        $this->assertDatabaseHas('matrices', [

            'first_matrix' => json_encode($payload['first_matrix'])

        ]);

    }

    /** @test */
    public function it_retrieves_matrices_successfully()
    {
        $response = $this->json('GET', '/api/v1/matrices', [], $this->header)
        ->assertStatus(200)
        ->assertJsonStructure([

            'message',
            'data'
        ]);
    }
}
